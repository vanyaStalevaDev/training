package algorithmicExercises;
/*
 * Given two strings calculate if you can modify with only one edit the first to become equal to the second. Return result - add, delete, change, nothing or impossible
 */

class CompareTwoStrings {
	private void differentSolution(String str1, String str2) {
		if (str1.equals(str2)) {
			System.out.println("Nothing");
			return;
		}
		int lengthDiff = str1.length() - str2.length();
		if (lengthDiff == -1 && checkDiffLength(str1, str2)) {
			System.out.println("Add");

		} else if (lengthDiff == 1 && checkDiffLength(str2, str1)) {
			System.out.println("Delete");

		} else if (lengthDiff == 0 && checkEqualLength(str1, str2)) {
			System.out.println("Change");
		} else {
			System.out.println("Impossible");
		}
	}

	private static boolean checkDiffLength(String str1, String str2) {
		int diffCount = 0;
		boolean oneLetterDiff = false;
		char[] charsStr1 = str1.toCharArray();
		char[] charsStr2 = str2.toCharArray();
		for (int i = 0; i < charsStr1.length; i++) {
			if (charsStr1[i - diffCount] != charsStr2[i]) {
				diffCount++;
			}
			if (diffCount > 1) {
				break;
			} else {
				oneLetterDiff = true;
			}
		}
		return oneLetterDiff;
	}

	private static boolean checkEqualLength(String str1, String str2) {
		int diffCount = 0;
		boolean oneLetterDiff = false;
		char[] charsStr1 = str1.toCharArray();
		char[] charsStr2 = str2.toCharArray();
		for (int i = 0; i < str1.length(); i++) {
			if (charsStr1[i] == charsStr2[i]) {
				continue;
			} else {
				diffCount++;
			}
			if (diffCount > 1) {
				oneLetterDiff = false;
				break;
			} else {
				oneLetterDiff = true;
			}
		}
		return oneLetterDiff;
	}

	public static void main(String[] args) {
		CompareTwoStrings twoStrings = new CompareTwoStrings();
		twoStrings.differentSolution("mice", "miice");
	}
}
