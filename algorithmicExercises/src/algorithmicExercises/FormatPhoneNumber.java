package algorithmicExercises;

/*Format a given telephone number with "-" making groups of 3 digits. For the last group/s if there are not enough digits,
*make groups of 2
*/

class FormatPhoneNumber {

	StringBuilder res = new StringBuilder();

	public StringBuilder solutionRecursion(String s) {
		if (s.length() < 5) {
			if (s.length() == 4) {
				res.append(s.substring(0, 2) + "-" + s.substring(2));
			} else
				res.append(s);
			return res;
		} else {
			res.append(s.substring(0, 3) + "-");
			res = solutionRecursion(s.substring(3));
		}
		return res;

	}

	public StringBuilder solutionItterative(String s) {
		StringBuilder res = new StringBuilder();
		while (s.length() > 4) {
			res.append(s.substring(0, 3));
			s = s.substring(3, s.length());
			res.append("-");
		}
		if (s.length() == 4) {
			res.append(s.substring(0, 2) + "-" + s.substring(2));

		} else {
			res.append(s);
		}

		return res;
	}

	public static void main(String[] args) {
		FormatPhoneNumber formatPhoneNumber = new FormatPhoneNumber();
		// String input = "1234567891";
		String input = "12345678";

		String newInput = input.replaceAll("[ -]+", "");

		StringBuilder result = formatPhoneNumber.solutionRecursion(newInput);
		StringBuilder result1 = formatPhoneNumber.solutionItterative(newInput);

		System.out.println("Result = " + result);
		System.out.println("Result1 = " + result1);

	}
}