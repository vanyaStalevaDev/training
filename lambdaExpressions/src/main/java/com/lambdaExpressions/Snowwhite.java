package com.lambdaExpressions;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Snowwhite {
	static Map<String, Integer> dwarfs = new LinkedHashMap<>();

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String command = scanner.nextLine();

		while (!command.equals("Once upon a time")) {
			String tokens[] = command.split(" <:> ");
			String dwarfName = tokens[0];
			String dwarfHatColor = tokens[1];
			int dwarfPhysics = Integer.parseInt(tokens[2]);

			if (dwarfs.containsKey(dwarfName + ":" + dwarfHatColor)) {
				chooseDwarfNew(dwarfName, dwarfHatColor, dwarfPhysics);
			} else {
				addDwarfNew(dwarfName, dwarfHatColor, dwarfPhysics);
			}

			command = scanner.nextLine();
		}
		scanner.close();
		printDwarfsNew();
	}

	private static void printDwarfsNew() {
		dwarfs.entrySet().stream().sorted((a, b) -> {
			int physicsA = a.getValue();
			int physicsB = b.getValue();
			int indexA = a.getKey().indexOf(":") + 1;
			int indexB = b.getKey().indexOf(":") + 1;

			String hatColorA = a.getKey().substring(indexA);
			String hatColorB = b.getKey().substring(indexB);
			int countA = countHatsColor(hatColorA);
			int countB = countHatsColor(hatColorB);
			if (physicsA == physicsB) {
				return countB - countA;
			}

			return physicsB - physicsA;
		}).forEach(e -> {
			String name = e.getKey().substring(0, e.getKey().indexOf(":"));
			String hatColor = e.getKey().substring(e.getKey().indexOf(":") + 1);
			System.out.printf("(%s) %s <-> %d%n", hatColor, name, e.getValue());
		});
	}

	private static int countHatsColor(String hatColor) {
		int counter = 0;
		for (Map.Entry<String, Integer> entry : dwarfs.entrySet()) {
			if (entry.getKey().contains(":" + hatColor)) {
				counter++;
			}
		}
		return counter;
	}

	private static void chooseDwarfNew(String name, String hatColor, int physics) {
		int currPhysics = dwarfs.get(name + ":" + hatColor);
		if (currPhysics < physics) {
			dwarfs.put(name + ":" + hatColor, physics);
		}

	}

	private static void addDwarfNew(String name, String hatColor, int physics) {

		dwarfs.put(name + ":" + hatColor, physics);
	}

}
