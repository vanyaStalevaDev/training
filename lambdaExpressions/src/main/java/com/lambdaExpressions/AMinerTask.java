package com.lambdaExpressions;
/* You are given a sequence of strings, each on a new line. Every odd line on the console is representing a resource
 *  (e.g. Gold, Silver, Copper, and so on), and every even – quantity. Your task is to collect the resources and print them each on a new line.
 *  Print the resources and their quantities in format: {resource} –> {quantity}
 *  The quantities inputs will be in the range [1 … 2 000 000 000]
 */

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class AMinerTask {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		String command = scanner.nextLine();
		Map<String, Long> counts = new LinkedHashMap<>();

		while (!command.equals("stop")) {
			String resource = command;

			long quantity = Long.parseLong(scanner.nextLine());
			if (counts.containsKey(resource)) {
				long currentQuantity = counts.get(resource);
				currentQuantity += quantity;
				counts.put(resource, currentQuantity);
			} else {
				counts.put(resource, quantity);
			}
			command = scanner.nextLine();
		}

		scanner.close();

		for (Map.Entry<String, Long> entry : counts.entrySet()) {
			System.out.printf("%s -> %d%n", entry.getKey(), entry.getValue());
		}

	}
}
