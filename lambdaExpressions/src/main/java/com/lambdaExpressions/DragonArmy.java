package com.lambdaExpressions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class DragonArmy {
	static Map<String, List<String>> dragons = new LinkedHashMap<>();
	static Map<String, List<Integer>> stats = new HashMap<>();
	static final int defaultDamage = 45;
	static final int defaultHealth = 250;
	static final int defaultArmor = 10;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int dragonsCount = Integer.parseInt(scanner.nextLine());

		for (int i = 0; i < dragonsCount; i++) {
			String[] tokens = scanner.nextLine().split(" ");
			String dragonType = tokens[0];
			String dragonName = tokens[1];
			int damage = 0;
			int health = 0;
			int armor = 0;

			if (!tokens[2].equals("null")) {
				damage = Integer.parseInt(tokens[2]);
			} else {
				damage = defaultDamage;
			}

			if (!tokens[3].equals("null")) {
				health = Integer.parseInt(tokens[3]);
			} else {
				health = defaultHealth;
			}

			if (!tokens[4].equals("null")) {
				armor = Integer.parseInt(tokens[4]);
			} else {
				armor = defaultArmor;
			}

			addDragon(dragonType, dragonName, damage, health, armor);

		}
		scanner.close();
		printDragons();

	}

	private static float avgStat(String[] names, int index) {
		float avgStat = 0;
		float sum = 0;
		int namesCount = names.length;
		for (Map.Entry<String, List<Integer>> entry : stats.entrySet()) {
			for (int i = 0; i < names.length; i++) {
				if (entry.getKey().equals(names[i])) {
					sum += entry.getValue().get(index);
				}

			}

		}
		avgStat = sum / namesCount;
		return avgStat;
	}

	private static void printDragons() {
		dragons.entrySet().stream().forEach(a -> {
			a.getValue().sort((c, d) -> c.compareTo(d));

			String[] names = stats.keySet().stream().filter(w -> a.getValue().contains(w)).toArray(String[]::new);
			float avgDamage = avgStat(names, 0);
			float avgHealth = avgStat(names, 1);
			float avgArmor = avgStat(names, 2);

			System.out.printf("%s::(%.2f/%.2f/%.2f)%n", a.getKey(), avgDamage, avgHealth, avgArmor);
			a.getValue().stream().forEach(f -> {
				int damage = stats.get(f).get(0);
				int health = stats.get(f).get(1);
				int armor = stats.get(f).get(2);
				System.out.printf("-%s -> damage: %d, health: %d, armor: %d%n", f, damage, health, armor);
			});

		});
	}

	private static void addDragon(String type, String name, int damage, int health, int armor) {
		List<String> names = null;
		if (dragons.containsKey(type)) {
			names = dragons.get(type);
		} else {
			names = new ArrayList<>();
		}
		if (!names.contains(name)) {
			names.add(name);
		}
		dragons.put(type, names);
		List<Integer> newStats = new ArrayList<>();
		newStats.add(damage);
		newStats.add(health);
		newStats.add(armor);
		stats.put(name, newStats);
	}

}
