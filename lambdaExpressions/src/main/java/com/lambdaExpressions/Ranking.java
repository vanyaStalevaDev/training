package com.lambdaExpressions;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Ranking {
	static Map<String, Map<String, Integer>> usersContestPoints = new HashMap<>();
	static Map<String, String> contests = new HashMap<>();

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		String command = scanner.nextLine();
		while (!command.equals("end of contests")) {
			String[] tokens = command.split(":");
			if (!contests.containsKey(tokens[0])) {
				contests.put(tokens[0], tokens[1]);
			}
			command = scanner.nextLine();
		}

		command = scanner.nextLine();
		while (!command.equals("end of submissions")) {
			String[] subTokens = command.split("=>");
			String contest = subTokens[0];
			String pass = subTokens[1];
			String userName = subTokens[2];
			int points = Integer.parseInt(subTokens[3]);
			if (checkValidContestAndPassword(contest, pass)) {
				if (usersContestPoints.containsKey(userName)) {
					Map<String, Integer> contestRes = usersContestPoints.get(userName);
					if (contestRes.containsKey(contest)) {
						if (contestRes.get(contest) < points) {
							updateContestPoints(points, contest, userName);
						}
					} else {// add the new contest to the user's contest and the points
						updateUserContests(points, contest, userName);
					}

				} else {// the user doesn't exist yet
					addContestPoints(points, contest, userName);
				}
			}
			command = scanner.nextLine();
		}
		scanner.close();

		printMaxPointsUser();
		System.out.println("Ranking:");

		usersContestPoints.entrySet().stream().sorted((a, b) -> a.getKey().compareTo(b.getKey())).forEach(a -> {
			System.out.println(a.getKey());
			a.getValue().entrySet().stream().sorted((c, d) -> d.getValue().compareTo(c.getValue()))
					.forEach(e -> System.out.printf("#  %s -> %d%n", e.getKey(), e.getValue()));
		});

	}

	private static void printMaxPointsUser() {
		int maxPoints = 0;
		String maxPointsUser = "";

		for (Map.Entry<String, Map<String, Integer>> entry : usersContestPoints.entrySet()) {
			Map<String, Integer> contestResults = entry.getValue();
			int sum = contestResults.values().stream().reduce(0, Integer::sum);
			if (sum > maxPoints) {
				maxPoints = sum;
				maxPointsUser = entry.getKey();
			}
		}
		System.out.printf("Best candidate is %s with total %d points.%n", maxPointsUser, maxPoints);
	}

	private static void updateUserContests(int points, String contest, String userName) {
		Map<String, Integer> contestRes = usersContestPoints.get(userName);
		contestRes.put(contest, points);

		usersContestPoints.put(userName, contestRes);
	}

	private static void addContestPoints(int points, String contest, String userName) {
		Map<String, Integer> contestRes = new HashMap<>();
		contestRes.put(contest, points);
		usersContestPoints.put(userName, contestRes);
	}

	private static void updateContestPoints(int points, String contestKey, String userName) {
		Map<String, Integer> contestRes = usersContestPoints.get(userName);
		contestRes.put(contestKey, points);

		usersContestPoints.put(userName, contestRes);
	}

	private static boolean checkValidContestAndPassword(String contest, String pass) {
		boolean result = false;
		if (contests.containsKey(contest)) {
			String checkPass = contests.get(contest);
			if (checkPass.equals(pass)) {
				result = true;
			}
		}
		return result;

	}

}
