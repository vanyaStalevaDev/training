package com.lambdaExpressions;
/*
 * Write a program, which keeps information about products and their prices. Each product has a name, a price and its quantity.
 *  If the product doesn’t exist yet, add it with its starting quantity.
	If you receive a product, which already exists increase its quantity by the input quantity and if its price is different, replace the price as well.
	You will receive products’ names, prices and quantities on new lines. Until you receive the command "buy", keep adding items. When you do receive the command "buy",
	print the items with their names and total price of all the products with that name. 

 */

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Orders {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		String command = scanner.nextLine();
		Map<String, Double> drinksPrice = new LinkedHashMap<>();
		Map<String, Integer> drinksQuantity = new LinkedHashMap<>();

		while (!command.equals("buy")) {
			String[] items = command.split("\\s");

			double newPrice = Double.parseDouble(items[1]);
			int quantity = Integer.parseInt(items[2]);
			if (drinksPrice.containsKey(items[0])) {
				int currentQuantity = drinksQuantity.get(items[0]);
				drinksPrice.put(items[0], newPrice);
				drinksQuantity.put(items[0], currentQuantity + quantity);
			} else {
				drinksPrice.put(items[0], newPrice);
				drinksQuantity.put(items[0], quantity);
			}
			command = scanner.nextLine();
		}

		for (Map.Entry<String, Integer> entry : drinksQuantity.entrySet()) {

			int quantity = entry.getValue();
			String drink = entry.getKey();
			double price = drinksPrice.get(drink);
			drinksPrice.put(drink, quantity * price);

		}
		drinksPrice.entrySet().stream().forEach(a -> System.out.printf("%s -> %.2f%n", a.getKey(), a.getValue()));
		scanner.close();
	}
}
